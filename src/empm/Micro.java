package empm;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Micro {
	private String destino;
	private Fecha f;
	private Double precioAsiento;
	private int capacidad;
	private HashMap<Integer, Butaca> butacas;
	private int id;
	public Micro(String destino2, Fecha f2, double precioButaca, int capacidad2) {
		this.destino = destino2;
		this.capacidad = capacidad2;
		this.f = f2;
		this.precioAsiento = precioButaca;
		id = actualizarCodigo();
		crearButacas(capacidad2);
		
	
	}

	private void crearButacas(int capacidad2) {
		Integer pos = 1;
		for (int i = 0; i < capacidad; i++) {
			Butaca b = new Butaca(pos);
			butacas.put(b.numeroAsiento(), b);
			
		}
	}

	private int actualizarCodigo() {
		return this.id++;
	}

	public void ocupacarAsiento(Integer numeroAsiento) {
		validadAsiento(numeroAsiento);
		Butaca b = butacas.get(numeroAsiento);
		b.ocuparAsiento();
	}
	private void validadAsiento(Integer numeroAsiento) {
		if(!butacas.containsKey(numeroAsiento)) {
			throw new RuntimeException("Posicion de asiento invalido");
		}
	}
	public int cantidadAsientosLibres() {
		return asientosLibres();
	}
	private int asientosLibres() {
		int cant = 0;
		for(Butaca b :butacas.values()) {
			if(b.asientoLibre()) {
				cant++;
			}
		}
		return cant;
	}
	private int cantidadAsientosOcupados() {
		return Math.abs(capacidad-asientosLibres());
	}
	public void liberarAsiento(Integer numeroAsiento) {
		validadAsiento(numeroAsiento);
		Butaca b = butacas.get(numeroAsiento);
		if(b.asientoLibre()) {
			throw new RuntimeException("Asiento libre, no es posible realizar la accion");
		}
		b.liberarAsiento();
	}
	public boolean asientoOcupado(Integer numeroAsiento) {
		Butaca b = butacas.get(numeroAsiento);
		return b.asientoLibre();
	}

	public Integer id() {
		return id;
	}

	public void cambiarMicro(Micro m2) {
		List<Butaca> b = butacasOcupadas();
		Integer pos = 0;
		while(m2.cantidadAsientosOcupados() !=0) {
			m2.ocupacarAsiento(pos);
			liberarAsiento(b.get(0).numeroAsiento());
			pos++;
		}
	}

	public List<Butaca> butacasOcupadas() {
	//	Set<Butaca> butacaOcupadas = new TreeSet<Butaca>();
		LinkedList<Butaca> butacaOcupadas = new LinkedList<>();
		for(Butaca b: butacas.values()) {
			if(!b.asientoLibre()) {
				butacaOcupadas.add(b);
			}
		}
		return butacaOcupadas;
	}
	
}
