package empm;

public class Fecha {
	Integer dia;
	Integer mes;
	Integer anio;
	Fecha(Integer dia, Integer mes, Integer anio){
		this.anio = anio;
		this.dia = dia;
		this.mes= mes;
	}
	public boolean esMenor(Fecha f) {
		return this.anio < f.getAnio() && this.mes < f.getMes() && 
				this.dia < f.getDia() ;
	}
	public Integer getDia() {
		return dia;
	}
	public void setDia(Integer dia) {
		this.dia = dia;
	}
	public Integer getMes() {
		return mes;
	}
	public Integer getAnio() {
		return anio;
	}
	
}
