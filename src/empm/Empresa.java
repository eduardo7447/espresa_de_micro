package empm;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class Empresa {
	HashMap<Integer,Micro> micros;

	/*---------Imprementacion---------*/
	private Empresa(){
		this.micros = new HashMap<>(); 
	}
	public void ocuparAsientoEspecifico(Integer codigoMicro, Integer numeroAsiento) {
		Micro m = validarCodMicro(codigoMicro);// si el micro no pertenece arroja una excepcion
		m.ocupacarAsiento(numeroAsiento);
	}
	private Micro validarCodMicro(Integer codigoMicro) {
		if(!micros.containsKey(codigoMicro)) {
			throw new RuntimeException("El id de micro no existe"+ codigoMicro);
		}
		return micros.get(codigoMicro);
	}
	public int cantidadDeAsientoLibres(Integer codigoMicro) {
		Micro m = validarCodMicro(codigoMicro);// si el micro no pertenece arroja una excepcion

		return m.cantidadAsientosLibres();
	}
	public List<Butaca> butacasLibres(Integer codigoMicro){
		Micro m = validarCodMicro(codigoMicro);// si el micro no pertenece arroja una excepcion
		return m.butacasOcupadas();
		
	}
	public void liberarAsiento(Integer codigoMicro, Integer numeroAsiento) {
		Micro m = validarCodMicro(codigoMicro);// si el micro no pertenece arroja una excepcion
		m.liberarAsiento(numeroAsiento);
	}
	public boolean asientoOcupado(Integer codigoMicro, Integer numeroAsiento) {
		Micro m = validarCodMicro(codigoMicro);// si el micro no pertenece arroja una excepcion
		return m.asientoOcupado(numeroAsiento);
	}
	public void agregarMicro(String destino, Fecha f,double precioButaca,int capacidad) {
		Micro m = new Micro(destino,f,precioButaca,capacidad);
		micros.put(m.id(),m);
		
	}
	public void cambiarMicro(Micro m1,Micro m2) {
		 validarCodMicro(m2.id());// si el micro no pertenece arroja una excepcion
		 validarCodMicro(m2.id());// si el micro no pertenece arroja una excepcion
		 m1.cambiarMicro(m2);
	}
	
	
	
	/*validaciones salvajes sin un test correspondientes*/
	public static void main(String[] args){
		System.out.print("sin error");
	}
	}
