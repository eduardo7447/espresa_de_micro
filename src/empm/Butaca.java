package empm;

public class Butaca {
	private Integer numeroAsiento;
	private boolean disponibilidad;
	Butaca(Integer numeroAsiento){
		this.numeroAsiento = numeroAsiento;
		this.disponibilidad = true;
	}
	public void ocuparAsiento() {
		disponibilidad = false;
	}
	public boolean asientoLibre() {
		// TODO Auto-generated method stub
		return disponibilidad;
	}
	public void liberarAsiento() {
		this.disponibilidad = true;	
	}
	public Integer numeroAsiento() {
		return this.numeroAsiento;
	}
	
}
